# Conjoiner

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/conjoiner`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'conjoiner'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install conjoiner

## Usage

After installing you can initialize the `prime` aspect with:

```
conjoiner init_aspect
```

This will create the dated repositories (gpx, log, misc) for the current year
and the `prime` aspect.

You can then manually synchronize those repositories with:
```
conjoiner sync
```

And show the status of the repositories with:
```
conjoiner show
```

You can also clone arbitrary repositories into the `joined` directory with:
```
conjoiner clone git@gitlab.com:acant/conjoiner.git
```

This repository would be cloned to `$HOME/joined/gitlab.com/acant/conjoiner`.


### Enable the synchronization deaemon on with systemd on Linux

You can enable the automatic synchronization with the following script:
```
mkdir -p ~/.config/systemd/user
cp conjoiner.service ~/.config/systemd/user
systemctl --user daemon-reload
systemctl --user enable conjoiner
systemctl --user start conjoiner
```

You can the use journalctl to view the logging with:
```
journalctl --user --unit=conjoiner --follow
```

### Enable the synchronization deaemon on with launchd on OSX

You can enable the automatic synchronization with the following script:
```
mkdir -p ~/Library/LaunchAgents
cp ca.andrewsullivancant.conjoiner.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/ca.andrewsullivancant.conjoiner.plist
```

You should be able to see logging with `Console.app`.


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on Gitlab at https://gitlab.com/acant/conjoiner.

## Similar projects

### Synchronization related
* https://github.com/nesquena/gitdocs
  - also has a [list of similar projects](https://github.com/nesquena/gitdocs#prior-projects)
* https://rubygems.org/gems/autovrsion
* https://tychoish.com/post/git-sync/
* https://github.com/RyanMcG/git-fyncy
* https://github.com/aanand/git-up
* https://github.com/tkellogg/dura

### Single repo commands
* https://github.com/mezis/git-whistles
* https://github.com/mhartl/git-utils
* https://kevquirk.com/git-commit-and-push-script/

### Multi-repository management
* https://myrepos.branchable.com/
  - also has a list of similar projects
* https://github.com/VirtusLab/git-machete
* https://github.com/Oakleon/git-join-repos
* https://github.com/lindell/multi-gitter

## License

Released under the [AGPLv3 license](https://www.gnu.org/licenses/agpl-3.0.en.html).

Copyright 2021 [Andrew Sullivan Cant](https://andrewsullivancant.ca)

