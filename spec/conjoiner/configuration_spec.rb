# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Conjoiner::Configuration do
  subject(:configuration) { described_class.new }

  describe '#aspects' do
    subject { configuration.aspects }

    it { is_expected.to eq(%w[prime sugar]) }
  end

  describe '#default_aspect', :fakefs do
    subject { configuration.default_aspect }

    context 'when default_aspect file exists' do
      before do
        Pathname.new(Dir.home).join('.config', 'conjoiner').mkpath
        Pathname.new(Dir.home).join('.config', 'conjoiner', 'default_aspect').write('other')
      end

      it { is_expected.to eq('other') }
    end

    context 'when no file exists' do
      it { is_expected.to eq('prime') }
    end
  end

  describe '#dated_repository_names' do
    subject { configuration.dated_repository_names(aspect) }

    inputs :aspect
    it_with 'prime', %w[gpx log misc]
    it_with 'other', %w[log]
  end
end
