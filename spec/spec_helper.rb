# frozen_string_literal: true

# HACK: require 'pp' to ensure that fakefs will load correctly.
# https://github.com/fakefs/fakefs#fakefs-----typeerror-superclass-mismatch-for-class-file
# https://stackoverflow.com/questions/37280343/why-does-fakefs-break-rspec
require 'pp'
require 'rspec/tabular'
require 'fakefs/spec_helpers'

RSpec.configure do |config|
  config.include FakeFS::SpecHelpers, fakefs: true
end

require 'simplecov'
SimpleCov.start

require 'conjoiner'
