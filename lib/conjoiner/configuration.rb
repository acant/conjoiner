# frozen_string_literal: true

module Conjoiner
  # @private
  # Frontend for determining the configuration of conjoiner. Either using
  # defaults specified in this file, or overridden by configuration located in:
  #   ~/.config/conjoiner/
  class Configuration
    # @return [Array<String>]
    def aspects
      %w[prime sugar]
    end

    # @return [String]
    def default_aspect
      return @default_aspect if @default_aspect

      default_aspect_pathname =
        Pathname.new(Dir.home).join('.config', 'conjoiner', 'default_aspect')

      @default_aspect =
        if default_aspect_pathname.readable?
          default_aspect_pathname.read.strip
        else
          'prime'
        end
    end

    # @param aspect [String]
    #
    # @return [Array<String>]
    def dated_repository_names(aspect)
      if aspect == 'prime'
        %w[gpx log misc]
      else
        %w[log]
      end
    end
  end
end
