# frozen_string_literal: true

require 'conjoiner/version'
require 'conjoiner/cli'
require 'conjoiner/configuration'

module Conjoiner
  class Error < StandardError; end
  # Your code goes here...
  #
end
