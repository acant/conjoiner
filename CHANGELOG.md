# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.2.1] - 2022-01-01
### Fixed
- `init_aspect` command failing on wrong number of argument when calling
  `Configuration#dated_repository_names`

## [0.2.0] - 2021-12-24
### Added
- Command to clone an arbitrary repository into the `~/joined` directory
  structure
- Command to list all the remote origins of the joined repositories
- Add configuration file for specifying the default aspect

## [0.1.0] - 2021-11-12
### Added
- Initializing dated aspect directories
- Manual synchronization command
- Show command to display git status of aspect directories
- Synchronization mode to watch aspect repositories, committing and pushing
  changes
