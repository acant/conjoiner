# frozen_string_literal: true

require_relative 'lib/conjoiner/version'

Gem::Specification.new do |spec|
  spec.name          = 'conjoiner'
  spec.version       = Conjoiner::VERSION
  spec.authors       = ['Andrew Sullivan Cant']
  spec.email         = ['mail@andrewsullivancant.ca']
  spec.summary       = 'Tool for cloning and managing git repositories.'
  spec.description   = 'Manager for cloned git reporitories, automatically sync some of those repositories.'
  spec.homepage      = 'https://gitlab.com/acant/conjoiner'
  spec.metadata      = {
    'homepage_uri'          => spec.homepage,
    'source_code_uri'       => spec.homepage,
    'changelog_uri'         => "#{spec.homepage}/CHANGELOG.md",
    'rubygems_mfa_required' => 'true'
  }
  spec.license       = 'AGPL-3.0-or-later'
  spec.files         =
    Dir.chdir(File.expand_path(__dir__)) do
      `git ls-files -z`.split("\x0").reject do |f|
        f.match(%r{^(test|spec|features)/})
      end
    end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.add_dependency 'git',           '~> 1.9'
  spec.add_dependency 'git_clone_url', '~> 2.0'
  spec.add_dependency 'listen',        '~> 3.6'
  spec.add_dependency 'table_print',   '~> 1.5'
  spec.add_dependency 'thor',          '~> 1.0'

  spec.add_development_dependency 'aruba',         '~> 1.0'
  spec.add_development_dependency 'cucumber',      '~> 5.3'
  spec.add_development_dependency 'fakefs',        '~> 1.4.0'
  spec.add_development_dependency 'rspec',         '~> 3.10'
  spec.add_development_dependency 'rspec-tabular', '~> 0.2'
  spec.add_development_dependency 'simplecov',     '~> 0.21'

  spec.add_development_dependency 'bundler-audit'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
  spec.add_development_dependency 'rubocop-rake'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency 'yard'
end
